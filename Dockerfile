FROM openshift/base-centos7

MAINTAINER Chris Timberlake <ctimberlake@gitlab.com>

LABEL io.k8s.description="OpenShift Tools for GitLab CI" \
      io.k8s.display-name="OC Tools For GitLab CI" \
      io.openshift.tags="builder,html,lighttpd"

RUN yum install -y epel-release && \
	yum install -y ansible wget && \
	yum clean all -y

RUN wget https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz && \
	tar -zvxf openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz && \
	cp openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit/oc /usr/local/bin && \
	rm -r openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit && \
	oc version